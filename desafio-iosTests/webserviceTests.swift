//
//  desafio_iosTests.swift
//  desafio-iosTests
//
//  Created by Edgar Cardoso on 4/2/16.
//  Copyright © 2016 Edgar Cardoso. All rights reserved.
//

import XCTest
@testable import desafio_ios

class webserviceTests: XCTestCase {
    
    var webService: WebService!
    
    override func setUp() {
        super.setUp()
        webService = WebService()
    }
    
    override func tearDown() {
        super.tearDown()
        webService = nil
    }
    
    func testGetRepos() {
        let expectation = expectationWithDescription("test will check repos is not empty")
        webService.getKotlinRepos { response in
            if let array = response?.repos {
                assert(!array.isEmpty, "array cannot be empty")
                expectation.fulfill()
            }
        }
        
        waitForExpectationsWithTimeout(10) { error in
            if error != nil {
                XCTFail("expectation failed")
            }
        }
    }
    
    //github only gives the first 1k results
    func testExceedThousandMarkRepos() {
        let expectation = expectationWithDescription("test will check repos is nil")
        webService.page = 100 //must be 35+
        webService.getKotlinRepos { response in
            assert(response?.repos == nil,"array must be nil")
            expectation.fulfill()
        }
        
        waitForExpectationsWithTimeout(10) { error in
            if error != nil {
                XCTFail("expectation timeout")
            }
        }
    }
    
    func testRepoPullRequests() {
        let expectation = expectationWithDescription("test will check pull request is not empty")
        webService.getPullRequestOfRepo("anko", user: "kotlin") { response in
            if let pullRequests = response {
                assert(!pullRequests.isEmpty, "pull requests cannot be empty")
                expectation.fulfill()
            }
        }
        
        waitForExpectationsWithTimeout(10) { error in
            if error != nil {
                XCTFail("expectation timeout")
            }
        }
    }
    
    func testRepoEmptyPullRequest() {
        let expectation = expectationWithDescription("test will check pull request is empty")
        webService.getPullRequestOfRepo("lwjgl3", user: "LWJGL") { response in
            assert(response?.count == 0, "pull requests must be empty")
            expectation.fulfill()
        }
        
        waitForExpectationsWithTimeout(10) { error in
            if error != nil {
                XCTFail("expectation timeout")
            }
        }
    }
    
    func testDownloadImage() {
        let expectation = expectationWithDescription("test will check image was downloaded")
        webService.downloadUserImage("https://avatars.githubusercontent.com/u/5282584?v=3") { image in
            expectation.fulfill()
        }
        
        waitForExpectationsWithTimeout(10) { error in
            if error != nil {
                XCTFail("expectation timeout")
            }
        }
    }
}
