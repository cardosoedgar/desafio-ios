//
//  NSDateExtensions.swift
//  desafio-ios
//
//  Created by Edgar Cardoso on 4/2/16.
//  Copyright © 2016 Edgar Cardoso. All rights reserved.
//

import Foundation

extension NSDate {
    func formatPullRequestDate() -> String {
        let dateFormater = NSDateFormatter()
        dateFormater.dateFormat = "dd/MM/yyyy"
        return dateFormater.stringFromDate(self)
    }
}