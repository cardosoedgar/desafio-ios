//
//  Repo.swift
//  desafio-ios
//
//  Created by Edgar Cardoso on 4/2/16.
//  Copyright © 2016 Edgar Cardoso. All rights reserved.
//
import Foundation
import ObjectMapper

class Repo: Mappable {

    var name: String!
    var description: String?
    var numberForks: Int!
    var numberStars: Int!
    var owner: User!
    
    required init?(_ map: Map) {
    }
    
    func mapping(map: Map) {
        name <- map["name"]
        description <- map["description"]
        numberForks <- map["forks_count"]
        numberStars <- map["stargazers_count"]
        owner <- map["owner"]
    }
}