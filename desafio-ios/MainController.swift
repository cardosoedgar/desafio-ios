//
//  MainController.swift
//  desafio-ios
//
//  Created by Edgar Cardoso on 4/2/16.
//  Copyright © 2016 Edgar Cardoso. All rights reserved.
//

import UIKit

class MainController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var tableView: UITableView!
    let webService = WebService()
    var repos = [Repo]()
    lazy var activityIndicator: UIActivityIndicatorView = {
        let spinner = UIActivityIndicatorView(activityIndicatorStyle: .Gray)
        spinner.startAnimating()
        spinner.color = UIColor.grayColor()
        spinner.hidesWhenStopped = true
        return spinner
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setLightStatusBarStyle()
        tableView.accessibilityLabel = "repoTableView"
        
        loadRepos()
    }
    
    func loadRepos() {
        self.tableView.tableFooterView = activityIndicator
        webService.getKotlinRepos { result in
            if let array = result?.repos {
                self.repos.appendContentsOf(array)
                self.tableView.reloadData()
            }
            
            self.tableView.tableFooterView = nil
        }
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return repos.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("repoCell") as! RepoCell
        let repo = repos[indexPath.row]
        let ownerAvatarUrl = NSURL(string: repo.owner.avatarUrl)!
        
        cell.nomeRepo.text = repo.name
        cell.nomeUsuario.text = repo.owner.login
        cell.descricaoRepo.text = repo.description
        cell.numeroFork.text = "\(repo.numberForks)"
        cell.numeroStar.text = "\(repo.numberStars)"
        cell.fotoUsuario.af_setImageWithURL(ownerAvatarUrl)
        
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let vc = storyboard?.instantiateViewControllerWithIdentifier("PullRequestController") as! PullRequestsController
        vc.repo = repos[indexPath.row]
        navigationController?.pushViewController(vc, animated: true)
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
    }
    
    func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        if indexPath.row == (self.repos.count - 1) {
            loadRepos()
        }
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
}
