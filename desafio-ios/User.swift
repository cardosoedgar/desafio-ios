//
//  User.swift
//  desafio-ios
//
//  Created by Edgar Cardoso on 4/2/16.
//  Copyright © 2016 Edgar Cardoso. All rights reserved.
//
import ObjectMapper
import Foundation

class User: Mappable {

    var login: String!
    var avatarUrl: String!
    
    required init?(_ map: Map) {
    }
    
    func mapping(map: Map) {
        login <- map["login"]
        avatarUrl <- map["avatar_url"]
    }
}