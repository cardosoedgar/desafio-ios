//
//  ViewControllerExtensions.swift
//  desafio-ios
//
//  Created by Edgar Cardoso on 4/2/16.
//  Copyright © 2016 Edgar Cardoso. All rights reserved.
//

import UIKit

extension UIViewController {
    
    func setLightStatusBarStyle() {
        navigationController?.navigationBar.barStyle = .Black
    }
}