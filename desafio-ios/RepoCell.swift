//
//  RepoCell.swift
//  desafio-ios
//
//  Created by Edgar Cardoso on 4/2/16.
//  Copyright © 2016 Edgar Cardoso. All rights reserved.
//

import UIKit

class RepoCell: UITableViewCell {
    
    @IBOutlet weak var fotoUsuario: UIImageView!
    @IBOutlet weak var nomeRepo: UILabel!
    @IBOutlet weak var nomeUsuario: UILabel!
    @IBOutlet weak var descricaoRepo: UILabel!
    @IBOutlet weak var numeroStar: UILabel!
    @IBOutlet weak var numeroFork: UILabel!
    
}
