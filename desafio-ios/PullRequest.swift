//
//  PullRequest.swift
//  desafio-ios
//
//  Created by Edgar Cardoso on 4/2/16.
//  Copyright © 2016 Edgar Cardoso. All rights reserved.
//
import Foundation
import ObjectMapper

class PullRequest: Mappable {
    
    var title: String!
    var body: String!
    var date: NSDate!
    var user: User!
    var url: String!
    
    required init?(_ map: Map) {
    }
    
    func mapping(map: Map) {
        title <- map["title"]
        body <- map["body"]
        date <- (map["created_at"], ISO8601DateTransform())
        user <- map["user"]
        url <- map["html_url"]
    }
}