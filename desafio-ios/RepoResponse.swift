//
//  Result.swift
//  desafio-ios
//
//  Created by Edgar Cardoso on 4/2/16.
//  Copyright © 2016 Edgar Cardoso. All rights reserved.
//

import Foundation
import ObjectMapper

class RepoResponse: Mappable {
    
    var repos: [Repo]?
    
    required init?(_ map: Map) {
    }
    
    func mapping(map: Map) {
        repos <- map["items"]
    }
}