//
//  WebService.swift
//  desafio-ios
//
//  Created by Edgar Cardoso on 4/2/16.
//  Copyright © 2016 Edgar Cardoso. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireImage
import ObjectMapper

typealias Response = (RepoResponse?) -> Void
typealias ResponseImage = (UIImage) -> Void
typealias PullRequestResponse = ([PullRequest]?) -> Void

class WebService {
    
    var page = 1
    
    func getKotlinRepos(completion: Response) {
        let url = "https://api.github.com/search/repositories?q=language:Kotlin&sort=stars&page=\(page)"
        Alamofire.request(.GET, url).responseJSON { response in
            if(response.result.isSuccess) {
                let result = Mapper<RepoResponse>().map(response.result.value)
                completion(result)
                self.page += 1
            }
        }
    }
    
    func downloadUserImage(url: String?, completion: ResponseImage) {
        if let imageUrl = url {
            Alamofire.request(.GET, imageUrl).responseImage { response in
                if let image = response.result.value {
                    completion(image)
                }
            }
        }
    }
    
    func getPullRequestOfRepo(repo: String, user: String, completion: PullRequestResponse) {
        let url = "https://api.github.com/repos/\(user)/\(repo)/pulls"
        Alamofire.request(.GET, url).responseJSON { response in
            if(response.result.isSuccess) {
                let result = Mapper<PullRequest>().mapArray(response.result.value)
                completion(result)
            }
        }
    }
}
