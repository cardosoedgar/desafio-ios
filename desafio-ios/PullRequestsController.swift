//
//  PullRequestsController.swift
//  desafio-ios
//
//  Created by Edgar Cardoso on 4/2/16.
//  Copyright © 2016 Edgar Cardoso. All rights reserved.
//

import UIKit

class PullRequestsController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var viewNoPullRequests: UIView!
    
    var repo: Repo!
    var pullRequests = [PullRequest]()
    let webService = WebService()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setLightStatusBarStyle()
        title = repo.name
        
        loadPullRequests()
    }
    
    func loadPullRequests() {
        webService.getPullRequestOfRepo(repo.name, user: repo.owner.login) { result in
            if result != nil {
                self.pullRequests.appendContentsOf(result!)
                self.tableView.reloadData()
                self.checkPullRequestsIsZero()
            }
        }
    }
    
    func checkPullRequestsIsZero() {
        if pullRequests.count == 0 {
            viewNoPullRequests.hidden = false
        } else {
            viewNoPullRequests.hidden = true
        }
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return pullRequests.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("pullRequestCell") as! PullRequestCell
        let pullRequest = pullRequests[indexPath.row]
        let ownerAvatarUrl = NSURL(string: pullRequest.user.avatarUrl)!
        
        cell.tituloPullRequest.text = pullRequest.title
        cell.bodyPullRequest.text = pullRequest.body
        cell.nomeUsuario.text = pullRequest.user.login
        cell.dataPullRequest.text = pullRequest.date.formatPullRequestDate()
        cell.fotoUsuario.af_setImageWithURL(ownerAvatarUrl)
        
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let pullRequestUrl = pullRequests[indexPath.row].url
        let url = NSURL(string: pullRequestUrl)!
        UIApplication.sharedApplication().openURL(url)
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
}
