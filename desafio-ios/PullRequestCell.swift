//
//  PullRequestCell.swift
//  desafio-ios
//
//  Created by Edgar Cardoso on 4/2/16.
//  Copyright © 2016 Edgar Cardoso. All rights reserved.
//

import UIKit

class PullRequestCell: UITableViewCell {

    @IBOutlet weak var tituloPullRequest: UILabel!
    @IBOutlet weak var bodyPullRequest: UILabel!
    @IBOutlet weak var nomeUsuario: UILabel!
    @IBOutlet weak var fotoUsuario: UIImageView!
    @IBOutlet weak var dataPullRequest: UILabel!
}
